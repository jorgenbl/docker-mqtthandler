FROM python:alpine3.6

RUN pip install requests
RUN pip install paho-mqtt

COPY mqtthandler.py /data/mqtthandler.py

VOLUME ["/data"] 

CMD ["python", "/data/mqtthandler.py"]